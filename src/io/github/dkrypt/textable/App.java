package io.github.dkrypt.textable;
import static io.github.dkrypt.textable.Cell.*;

public class App {
    public static void main(String[] args) {
        Textable table = new Textable.Builder()
                .tableName("myTable")
                .align(ALIGN.LEFT)
                .borderStyle("hippie")
                .columns(3)
                .autoAdjustWidth(false)
                .isHeaderLess(false)
                .build();

        Header header = new Header.Builder()
                .addField(new Header.Field(ALIGN.CENTER, 3, "S.No."))
                .addField(new Header.Field(ALIGN.CENTER, 10, "Jar Name"))
                .addField(new Header.Field(ALIGN.CENTER, 20, "Directory"))
                .build();

        table.addHeader(header);
        Row row = new Row.Builder().addData("1").addData("nifi-processor-1.8.0.jar").addData("/opt/shared/nifi/maven").build();
        table.insertRow(row);
    }
}
