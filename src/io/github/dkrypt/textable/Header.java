/**
 * Copyright 2020 dkrypt.github.io
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.github.dkrypt.textable;

import java.util.ArrayList;
import java.util.List;

public class Header {

    private List<Field> fields;

    public Header(Builder builder){
        this.fields = builder.fields;
    }

    public List<Field> getFields() {
        return fields;
    }

    public static class Builder {
        private List<Field> fields = new ArrayList<>();

        public Builder addField(Field field){
            if(null != field)
                this.fields.add(field);
            return this;
        }

        public Header build(){
            return new Header(this);
        }
    }

    public static class Field {
        protected Cell.ALIGN alignment;
        protected int padding;
        protected String fieldName;

        public Field(Cell.ALIGN alignment, int padding, String fieldName) {
            this.alignment = alignment;
            this.padding = padding;
            this.fieldName = fieldName;
        }
    }
}
