/**
 * Copyright 2020 dkrypt.github.io
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.github.dkrypt.textable;

import java.util.ArrayList;
import java.util.List;

public class Row {
    List<String> dataList;

    /**
     * Public constructor using builder instance.
     * @param builder
     */
    public Row(Builder builder)
    {
        this.dataList = builder.dataList;
    }

    @Override
    public String toString() {
        String rowText = "|";
        for(String data : dataList){
            rowText += rowText.concat()
        }
    }

    public static class Builder {
        List<String> dataList = new ArrayList<>();

        /**
         * Adds string data to row
         * @param data data you want to insert into row.
         * @return Row.Builder
         */
        public Builder addData(String data)
        {
            dataList.add(data);
            return this;
        }

        /**
         * Builds and returns the Row instance.
         * @return Row
         */
        public Row build()
        {
            return new Row(this);
        }
    }

}
