/**
 * Copyright 2020 dkrypt.github.io
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.github.dkrypt.textable;

import io.github.dkrypt.textable.util.FormatUtils;

import java.util.List;

public class Textable {
    private Header header;
    private List<Row> dataList;
    private TextableConfig tableConfig = null;
    private String tableName;
    private int columns;
    private Cell.ALIGN align;
    private boolean isHeaderLess;
    private String borderStyle;
    private boolean autoAdjustWidth;

    /**
     * Public constructor which is initialised by the Builder.
     * @param builder Textable.Builder object.
     */
    public Textable(final Builder builder) {
        this.tableName = builder.tableName;
        this.columns = builder.columns;
        this.align = builder.align;
        this.isHeaderLess = builder.isHeaderLess;
        this.borderStyle = builder.borderStyle;
        this.autoAdjustWidth = builder.autoAdjustWidth;
        setConfig();
    }

    /**
     * Private method to set config for table.
     */
    private void setConfig()
    {
        tableConfig = new TextableConfig();
        tableConfig.setTableName(this.tableName);
        tableConfig.setColumns(this.columns);
        tableConfig.setAlign(this.align);
        tableConfig.setHeaderLess(this.isHeaderLess);
        tableConfig.setBorderStyle(this.borderStyle);
        tableConfig.setAutoAdjustWidth(this.autoAdjustWidth);
    }

    public String text()
    {
        String table = this.header.toString();

        for(Row row : dataList) {
            table = table.concat(row.toString());
        }
        return table;
    }

    /**
     * Adds Header object to the table
     * @param header Header object
     */
    public void addHeader(Header header){
        this.header = header;
        processMathsForTable();
    }

    public void processMathsForTable()
    {
        List<Header.Field> fields = this.header.getFields();
        int width = fields.size() + 1;
        for(Header.Field field : fields){
            int pad = (field.alignment.equals(Cell.ALIGN.CENTER)) ? 2*field.padding : field.padding;
            width += field.fieldName.length() + pad;
        }
        this.tableConfig.setWidth(width);
    }

    private String formattedHeader(List<Header.Field> fields)
    {
        String headerRow = "";
        String verticalSeparator = this.tableConfig.getVerticalSeparator();
        String border = this.tableConfig.getBorder();
        for(Header.Field field: fields){
            String formattedField = formatField(field);
            headerRow = headerRow.concat(verticalSeparator).concat(formattedField);
        }
        headerRow = headerRow.concat(verticalSeparator);
        String borderStr = new String(new char[tableConfig.getWidth()]).replace("\0",border);
        return borderStr.concat("\n").concat(headerRow).concat("\n").concat(borderStr);
    }

    private String formatField(Header.Field field)
    {
        final int padding = field.padding;
        if(field.alignment.equals(Cell.ALIGN.CENTER)){
            return FormatUtils.padAndCenterString(field.fieldName, padding);
        }
//        else if(field.alignment.equals(Cell.ALIGN.right)){
//            return FormatUtils.padAndCenterString(field.fieldName, padding);
//
//        }else if(field.alignment.equals(Cell.ALIGN.left)){
//            return FormatUtils.padAndCenterString(field.fieldName, padding);
//
//        }
        return FormatUtils.padAndCenterString(field.fieldName, padding);
    }
    /**
     * Inserts a list of Row objects, as separate Rows
     * into the table.
     * @param rowList List of Row objects
     */
    public void insertRows(List<Row> rowList)
    {
        this.dataList = rowList;
    }

    /**
     * Insert a Row object into the table.
     * @param row Row object
     */
    public void insertRow(Row row)
    {
        this.dataList.add(row);
    }

    /**
     * Inner Builder class to implement builder pattern.
     */
    public static final class Builder {
        private String tableName = null;
        private int columns = 0;
        private Cell.ALIGN align = Cell.ALIGN.LEFT;
        private boolean isHeaderLess = false;
        private String borderStyle = "box-drawing";
        private boolean autoAdjustWidth = true;

        /**
         * Sets table name
         * @param tableName name of the table
         * @return the builder
         */
        public Builder tableName(String tableName){
            if(null != tableName)
                this.tableName = tableName;
            return this;
        }

        /**
         * Sets number of columns
         * @param columns number of columns
         * @return the builder
         */
        public Builder columns(int columns){
            if(0 != columns)
                this.columns = columns;
            return this;
        }

        /**
         * Sets alignment for all cells of table
         * @param align cell alignment
         * @return the builder
         */
        public Builder align(Cell.ALIGN align){
            if(null != align)
                this.align = align;
            return this;
        }

        /**
         * Specifies whether this table is header-less or not.
         * @param isHeaderLess true if header-less
         * @return the builder
         */
        public Builder isHeaderLess(boolean isHeaderLess){
            this.isHeaderLess = isHeaderLess;
            return this;
        }

        /**
         * Sets border style for the table.
         * @param borderStyle one of [hippie, box-drawing, stars, maths]
         * @return the builder
         */
        public Builder borderStyle(String borderStyle){
            if(null!=borderStyle)
                this.borderStyle = borderStyle;
            return this;
        }

        /**
         * Sets if width should be managed automatically.
         * @param autoAdjustWidth true if auto adjust width is enabled
         * @return the builder
         */
        public Builder autoAdjustWidth(boolean autoAdjustWidth){
            this.autoAdjustWidth = autoAdjustWidth;
            return this;
        }

        public Textable build(){
            if(tableName == null){
                throw new IllegalStateException("Must specify a table name.");
            }
            if(columns == 0){
                throw new IllegalStateException("Must specify at least one column(s)");
            }
            return new Textable(this);
        }
    }
}
