/**
 * Copyright 2020 dkrypt.github.io
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.github.dkrypt.textable;

public class TextableConfig {
    private String tableName;
    private int columns;
    private Cell.ALIGN align;
    private boolean isHeaderLess;
    private String borderStyle;
    private boolean autoAdjustWidth;
    private int width;

    public String getVerticalSeparator()
    {
        if(this.borderStyle.contentEquals("hippie"))
            return "|";
        if(this.borderStyle.contentEquals("box-drawing"))
            return Character.toString(0x2551);
        return "|";
    }
    public String getBorder()
    {
        if(this.borderStyle.contentEquals("hippie"))
            return "=";
        if(this.borderStyle.contentEquals("box-drawing"))
            return Character.toString(0x2550);
        return "=";
    }
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public Cell.ALIGN getAlign() {
        return align;
    }

    public void setAlign(Cell.ALIGN align) {
        this.align = align;
    }

    public boolean isHeaderLess() {
        return isHeaderLess;
    }

    public void setHeaderLess(boolean headerLess) {
        isHeaderLess = headerLess;
    }

    public String getBorderStyle() {
        return borderStyle;
    }

    public void setBorderStyle(String borderStyle) {
        this.borderStyle = borderStyle;
    }

    public boolean isAutoAdjustWidth() {
        return autoAdjustWidth;
    }

    public void setAutoAdjustWidth(boolean autoAdjustWidth) {
        this.autoAdjustWidth = autoAdjustWidth;
    }
}
