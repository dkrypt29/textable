This Textable Project is aimed to be built as Textual Table Printer, which renders data in tabular form, as a string.


Textable (C)
    |_____Header (C)
    |       + A Simple Complex Data Type Which only handles "Raw Header Data".
    |       + No Formatting shall be done in this class.
    |           Formatting shall be done on the basis of configured Table properties in Textable Class.
    |_____Row (C)
            + A Simple Complex Data Type Which only handles "Raw Row Data".
            + No Formatting shall be done in this class.